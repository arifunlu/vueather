# vueather

> A Vue.js weather app demo

## Before you start

Grab your free API key at http://openweathermap.org/

Paste the API key in `/src/constants/api.js`

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
