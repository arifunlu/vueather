import Vue from 'vue';
import VueRouter from 'vue-router';
import Cities from './components/Cities';
import NewCity from './components/NewCity';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Cities },
  { path: '/add', component: NewCity },
];

export default new VueRouter({
  mode: 'history',
  routes,
});
