export default {
  weatherKey: '',
  weatherUrl: 'http://api.openweathermap.org/data/2.5/weather?q=__CITY__&units=imperial&appid=__KEY__',

  urlForCity(city) {
    return this.weatherUrl.replace('__CITY__', city).replace('__KEY__', this.weatherKey);
  },
};
