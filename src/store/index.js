/* global fetch */

import Vuex from 'vuex';
import Vue from 'vue';
import _ from 'lodash';
import apiKeys from '../constants/api';

Vue.use(Vuex);

// eslint-disable-next-line new-cap
const store = new Vuex.Store({
  state: {
    cities: [],
    forecast: {},
  },
  getters: {
    hasCities: state => state.cities.length > 0,
  },
  mutations: {
    addCity(state, cityName) {
      if (_.includes(state.cities, cityName)) {
        return;
      }
      state.cities.push(cityName);
      Vue.set(state.forecast, cityName, false);
    },
    setWeather(state, weather) {
      if (!weather.city || !weather.data) {
        return;
      }
      Vue.set(state.forecast, weather.city, weather.data);
    },
  },
  actions: {
    requestForecast({ commit }, cityName) {
      if (cityName === '') {
        return;
      }
      commit('addCity', cityName);
      fetch(apiKeys.urlForCity(cityName))
        .then(res => res.json())
        .then((json) => {
          if (!json.main) {
            throw new Error('No Weather Data');
          }
          return {
            temp: json.main.temp,
            temp_min: json.main.temp_min,
            temp_max: json.main.temp_max,
          };
        })
        .then((data) => {
          commit('setWeather', {
            city: cityName,
            data,
          });
        })
        .catch(() => {
          commit('setWeather', {
            city: cityName,
            data: false,
          });
        });
    },
  },
});

export default store;
